package com.kindev.paginationsample

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val TABLE_NAME = "entities"
const val VALUE_1_COLUMN_NAME = "value1"
const val VALUE_2_COLUMN_NAME = "value2"


@Entity(tableName = TABLE_NAME)
data class Entity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    @ColumnInfo(name = VALUE_1_COLUMN_NAME)
    val value1: String,
    @ColumnInfo(name = VALUE_2_COLUMN_NAME)
    val value2: String)