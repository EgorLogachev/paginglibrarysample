package com.kindev.paginationsample.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.kindev.paginationsample.Entity
import com.kindev.paginationsample.data.database.DatabaseService
import com.kindev.paginationsample.data.network.NetworkService

interface Repository {

    companion object Factory {
        fun getInstance(context: Context): Repository = RepositoryImpl.getInstance(NetworkService.getInstance(), DatabaseService.getInstance(context))
    }

    fun getEntities(pageSize: Int): LiveData<Result<List<Entity>>>
}

private class RepositoryImpl(private val network: NetworkService,
                             private val database: DatabaseService) : Repository {

    companion object {

        private var instance: RepositoryImpl? = null

        fun getInstance(networkService: NetworkService, dbService: DatabaseService): RepositoryImpl {
            return instance ?: synchronized(this) {
                instance ?: RepositoryImpl(networkService, dbService).also {
                    instance = it
                }
            }
        }
    }

    override fun getEntities(pageSize: Int): LiveData<Result<List<Entity>>> {
        /**
         * create observable result entity that will be observed from view model
         */
        val resultData = MediatorLiveData<Result<List<Entity>>>()

        /**
         * observe number of page that data source load from database and fetch data from server for this page
         */
        val pageData = MutableLiveData<Int>()
        resultData.addSource(pageData) {
            fetchEntities(it, pageSize, resultData)
        }
        /**
         * receive observable paged list from database and map it on result
         */
        val entities = database.getEntities(pageSize, pageData)
        resultData.addSource(entities) {
            resultData.value = SuccessResult(it)
        }

        /**
         * return observable result to view model
         */
        return resultData
    }

    private fun fetchEntities(page: Int, pageSize: Int, resultData: MediatorLiveData<Result<List<Entity>>>) {
        val networkData = network.getEntities(page, pageSize)

        /**
         * observe network data
         */
        resultData.addSource(networkData) {
            when (it) {
                is SuccessResult<List<Entity>> -> {
                    database.insert(it.data)
                    resultData.value = ResultLoaded
                }
                else -> resultData.value = it
            }
        }

    }
}
