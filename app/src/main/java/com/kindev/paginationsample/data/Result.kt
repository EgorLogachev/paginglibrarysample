package com.kindev.paginationsample.data

sealed class Result<out T>
data class SuccessResult<T>(val data: T) : Result<T>()
class FailureResult(val e: Exception) : Result<Nothing>()
object LoadingResult : Result<Nothing>()
object ResultLoaded : Result<Nothing>()
