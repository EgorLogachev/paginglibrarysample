package com.kindev.paginationsample.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kindev.paginationsample.Entity

@Database(entities = [Entity::class], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract fun getEntitiesDao(): EntitiesDao
}