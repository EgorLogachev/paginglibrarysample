package com.kindev.paginationsample.data.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.room.Room
import com.kindev.paginationsample.Entity
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

private const val DB_NAME = "entities_database"

interface DatabaseService {

    companion object Factory {
        fun getInstance(context: Context): DatabaseService = DatabaseServiceImpl(context)
    }

    fun insert(entities: List<Entity>)

    fun getEntities(pageSize: Int, observablePage: MutableLiveData<Int>): LiveData<PagedList<Entity>>

}

private class DatabaseServiceImpl(context: Context) : DatabaseService {

    private val db: Database = Room.databaseBuilder(context, Database::class.java, DB_NAME).build()
    private val executor: ExecutorService = Executors.newSingleThreadExecutor()

    override fun insert(entities: List<Entity>) {
        executor.execute {
            db.getEntitiesDao().insert(entities)
        }
    }

    override fun getEntities(pageSize: Int, observablePage: MutableLiveData<Int>): LiveData<PagedList<Entity>> {
        /**
         * config paged list
         */
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .setPrefetchDistance(pageSize)
            .setEnablePlaceholders(false)
            .build()

        /**
         * receive paged list from database
         */
        return LivePagedListBuilder(object : DataSource.Factory<Int, Entity>() {
            override fun create() = EntitiesDataSource(db.getEntitiesDao().getEntities(), observablePage)
        }, config).setFetchExecutor(executor).build()
    }
}