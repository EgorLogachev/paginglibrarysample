package com.kindev.paginationsample.data.database

import androidx.paging.DataSource
import androidx.room.*
import com.kindev.paginationsample.Entity

@Dao
interface EntitiesDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entities: List<Entity>)

    @Query("SELECT * FROM entities")
    fun getEntities(): DataSource.Factory<Int, Entity>
}