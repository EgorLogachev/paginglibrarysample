package com.kindev.paginationsample.data.database

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PositionalDataSource
import com.kindev.paginationsample.Entity

private const val INITIAL_PAGE = 0

class EntitiesDataSource(roomDataSourceFactory: DataSource.Factory<Int, Entity>,
    private val pageData: MutableLiveData<Int>
) : PositionalDataSource<Entity>() {

    private val dataSource = (roomDataSourceFactory.create() as PositionalDataSource)

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Entity>) {
        pageData.postValue(INITIAL_PAGE)
        dataSource.loadInitial(params, callback)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Entity>) {
        pageData.postValue(params.startPosition/params.loadSize)
        dataSource.loadRange(params, callback)
    }
    
    override fun addInvalidatedCallback(onInvalidatedCallback: InvalidatedCallback) {
        super.addInvalidatedCallback(onInvalidatedCallback)
        dataSource.addInvalidatedCallback(onInvalidatedCallback)
    }

    override fun removeInvalidatedCallback(onInvalidatedCallback: InvalidatedCallback) {
        super.removeInvalidatedCallback(onInvalidatedCallback)
        dataSource.removeInvalidatedCallback(onInvalidatedCallback)
    }

    override fun invalidate() {
        super.invalidate()
        dataSource.invalidate()
    }

    override fun isInvalid(): Boolean {
        return super.isInvalid() || dataSource.isInvalid
    }
}

