package com.kindev.paginationsample.data.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kindev.paginationsample.Entity
import com.kindev.paginationsample.data.LoadingResult
import com.kindev.paginationsample.data.Result
import com.kindev.paginationsample.data.SuccessResult
import java.util.concurrent.Executors

interface NetworkService {

    companion object Factory {
        fun getInstance(): NetworkService = NetworkServiceImpl
    }

    fun getEntities(page: Int, pageSize: Int): LiveData<Result<List<Entity>>>
}

private object NetworkServiceImpl: NetworkService {

    private val executor = Executors.newSingleThreadExecutor()

    override fun getEntities(page: Int, pageSize: Int): LiveData<Result<List<Entity>>> {
        val resultData = MutableLiveData<Result<List<Entity>>>()
        resultData.postValue(LoadingResult)
        executor.execute {
            val entities = MutableList(pageSize) {
                val entityId = (it + page * pageSize + 1).toLong()
                val value1 = "Entity title $entityId"
                val value2 = "Entity subtitle $entityId"
                Entity(entityId, value1, value2)
            }
            Thread.sleep(500)
            resultData.postValue(SuccessResult(entities))
        }
        return resultData
    }
}

