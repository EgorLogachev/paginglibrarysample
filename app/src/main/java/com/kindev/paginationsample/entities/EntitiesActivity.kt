package com.kindev.paginationsample.entities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kindev.paginationsample.R

class EntitiesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entities)
    }
}
