package com.kindev.paginationsample.entities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kindev.paginationsample.Entity
import com.kindev.paginationsample.databinding.ItemEntityBinding

class EntitiesAdapter(private val inflater: LayoutInflater): PagedListAdapter<Entity, EntityViewHolder>(DIFF_UTIL) {

    companion object {
        val DIFF_UTIL = object: DiffUtil.ItemCallback<Entity>() {
            override fun areItemsTheSame(oldItem: Entity, newItem: Entity) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Entity, newItem: Entity) = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        val binding = ItemEntityBinding.inflate(inflater, parent, false).apply {
            viewModel = EntityViewModel()
        }
        return EntityViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}


class EntityViewHolder(private val binding: ItemEntityBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(entity: Entity?) {
        binding.viewModel?.start(entity)
    }
}