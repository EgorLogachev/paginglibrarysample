package com.kindev.paginationsample.entities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import com.kindev.paginationsample.Entity
import com.kindev.paginationsample.databinding.FragmentEntitiesBinding

class EntitiesFragment : Fragment() {

    private lateinit var binding: FragmentEntitiesBinding
    private val adapter by lazy { EntitiesAdapter(LayoutInflater.from(context)) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEntitiesBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(activity!!)[EntitiesViewModel::class.java]
            viewModel?.entities?.observe(this@EntitiesFragment, Observer {
                adapter.submitList(it as PagedList<Entity>?)
            })
        }
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.recyclerView.adapter = adapter
        binding.viewModel?.start()
    }
}
