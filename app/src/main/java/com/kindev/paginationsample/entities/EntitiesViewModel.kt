package com.kindev.paginationsample.entities

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kindev.paginationsample.Entity
import com.kindev.paginationsample.data.*

private const val PAGE_SIZE = 50

class EntitiesViewModel(application: Application) : AndroidViewModel(application) {

    private val repo = Repository.getInstance(application.applicationContext)
    private val entitiesData by lazy { repo.getEntities(PAGE_SIZE) }
    val isProgress = MutableLiveData<Boolean>()
    val entities = MutableLiveData<List<Entity>>()

    private val changeListener: (Result<List<Entity>>) -> (Unit) = {
        when (it) {
            is SuccessResult<List<Entity>> -> {
                entities.value = it.data
                isProgress.value = false
            }
            is FailureResult -> {
                showToast("Error: ${it.e.message}")
                isProgress.value = false
            }
            is LoadingResult -> {
                isProgress.value = (entities.value.isNullOrEmpty())
            }
            is ResultLoaded -> {
                isProgress.value = false
            }
        }
    }


    fun start() {
        entitiesData.observeForever(changeListener)
    }

    private fun showToast(text: String) {
        Toast.makeText(getApplication(), text, Toast.LENGTH_SHORT).show()
    }

    override fun onCleared() {
        entitiesData.removeObserver(changeListener)
        super.onCleared()
    }
}