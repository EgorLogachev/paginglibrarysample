package com.kindev.paginationsample.entities

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.kindev.paginationsample.Entity

class EntityViewModel: ViewModel() {

    val title = ObservableField<String>()
    val subtitle = ObservableField<String>()

    fun start(entity: Entity?) {
        entity?.let {
            title.set(it.value1)
            subtitle.set(it.value2)
        }
    }

}